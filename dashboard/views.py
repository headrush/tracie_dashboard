from django.shortcuts import render
from dashboard.models import TraceResult, PipelineResult
import util.ci as ci

def _mesa_url(commit):
    if commit is None:
        return None
    else:
        return "https://gitlab.freedesktop.org/mesa/mesa/commit/" + commit

def _to_trace_result(pid, job, name):
    result = job.trace_results.get(name, "")

    if result == "match":
        tr_status = "pass"
    elif result == "differ":
        tr_status = "fail"
    else:
        tr_status = "unknown"

    return TraceResult(
            summary_url = ci.trace_summary_url(job.id, name),
            status = tr_status,
            name = name,
            pipeline_result_id = pid)

def _to_pipeline_result(pipeline):
    if pipeline.status == "success":
        rstatus = "pass"
    elif pipeline.status == "failed":
        rstatus = "fail"
    else:
        rstatus = "unknown"

    return PipelineResult(
            id = pipeline.id,
            url = ci.pipeline_url(pipeline.id),
            mesa_sha = pipeline.mesa_sha,
            mesa_url = _mesa_url(pipeline.mesa_sha),
            status = rstatus,
            test_job_id = pipeline.test_job_id,
            triggered_by_mesa_push = pipeline.triggered_by_mesa_push)

def _pipeline_result_for_id(pid):
    db_pr = PipelineResult.objects.filter(id=pid)
    if db_pr:
        return db_pr[0]

    pipeline = ci.get_pipeline(pid)
    presult = _to_pipeline_result(pipeline)
    if pipeline.status != "running" and pipeline.status != "pending":
        presult.save()

    return presult

def _trace_results_for_id(pid):
    pipeline_result = _pipeline_result_for_id(pid)
    if pipeline_result.test_job_id is None:
        return []

    db_tr = TraceResult.objects.filter(pipeline_result=pid)
    if db_tr:
        return sorted(db_tr, key=lambda tr: tr.name)

    test_job = ci.get_test_job(pipeline_result.test_job_id)
    traces_sorted = sorted(test_job.trace_results.keys())

    trace_results = [_to_trace_result(pid, test_job, t) for t in traces_sorted]

    if pipeline_result.status != "unknown":
        for tresult in trace_results:
            tresult.save()

    return trace_results

def landing(request):
    pids = ci.get_pipeline_ids()

    all_pipeline_results = [_pipeline_result_for_id(pid) for pid in pids]
    pipeline_results = [pr for pr in all_pipeline_results if pr.triggered_by_mesa_push]

    context = {"pipeline_results" : pipeline_results}

    return render(request, "dashboard/landing.html", context)

def results(request, pid):
    pipeline_result = _pipeline_result_for_id(pid)
    trace_results = _trace_results_for_id(pid)

    context = {"pipeline_result" : pipeline_result, "trace_results": trace_results}

    return render(request, "dashboard/results.html", context)
