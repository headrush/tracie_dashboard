FROM tiangolo/uwsgi-nginx:python3.6-alpine3.9
LABEL maintainer="Alexandros Frantzis <alexandros.frantzis@collabora.com>"

# Copy the website to the /app folder
COPY . /app/

# Install all our dependencies
WORKDIR /app
RUN pip install -r requirements.txt
